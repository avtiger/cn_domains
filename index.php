<?php
session_start();

	if (!$_SESSION['auth']) {
		//$_SESSION['auth'] =  true;
		header("Location:login.php");
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>網域偵測</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap.min.css">
	<script src="jquery.min.js"></script>
	<script src="popper.min.js"></script>
	<script src="bootstrap.min.js"></script>
		<link rel="stylesheet" href="layer/3.1.0/theme/default/layer.css" />
		<script src="layer/3.1.0/layer.js"></script>
</head>

<style>
	.btnstyle .btn {margin:10px 0}
	#tctb .titleFont {	font-size:20px;	font-weight:normal}
	#tctb .unusualContentFont {	font-size:15px;	font-weight:normal; color:red}
	#tctb .normalContentFont {	font-size:15px;	font-weight:normal; color:blue}
	
</style>

<body>

	<div class="container">
		<h2>檢查網域名稱</h2>
		<p class="lead">By 4hu.com</p>
		<div class="form-group">
			<label for="comment">請輸入域名, 目前<span id="dnsCount">0</span>個 :<span class="process-span" style="display:none;">已處理 <span class="nums">0</span> 筆 <img src="http://ping.twdio.com/images/loader.gif" width="25"></span> </label>
			<textarea class="form-control" rows="7" name="text" id="fname"><?= file_get_contents('lists.txt'); ?></textarea>
		</div>

		<div class="row btnstyle my-group">
			<div class="col-md-6 col-xs-12">
				<button type="button" class="btn btn-success btn-block" value="poison" onclick="app.submit('poison')">汙染查詢</button>
			</div>
			<div class="col-md-6 col-xs-12">
				<button type="button" class="btn btn-primary btn-block" value="wall" onclick="app.submit('wall')">中國城牆</button>
			</div>
		</div>


		<div id="static_div" style="display:none">
			<div class="form-group col-md-12">
				<label for="check_gfw_domains" title="这里是:问题域名"><span id='tpxx'></span>域名:(<span id="check_gfw_domains_status">0</span>)</label>
				<textarea class="form-control" rows="5" id="check_gfw_domains" placeholder="" readonly></textarea>
			</div>
			<div class="form-group col-md-12">
				<label for="check_nogfw_domains" title="这里是:正常域名">正常域名:(<span id="check_nogfw_domains_status">0</span>)</label>
					<div class="cont">
						<textarea class="form-control" rows="5" id="check_nogfw_domains" placeholder="" readonly></textarea>
					</div>

			</div>

			<div class="form-group col-md-12" >
				<label for="check_disgfw_domains" title="疑似被墙,请重新查询">疑似被封:(<span id="check_disgfw_domains_status">0</span>)</label>
				<textarea class="form-control" rows="1" id="check_disgfw_domains" placeholder="" readonly></textarea>
			</div>

			<div class="btnstyle my-group">
				<div class="col-md-12 col-xs-12" style="">
					<button type="button" class="btn btn-info btn-block" id="download" value="" onclick="app.download()">下載</button>
				</div>
			</div>
		</div>


	</div>

	<iframe id="iframe" style="display:none;"></iframe>

	<form method="post" id="downloadForm" action="download.php">
		<input type="hidden" class="check_download1" name="check_gfw_domains"> 	
		<input type="hidden" class="check_download2" name="check_nogfw_domains"> 	
		<input type="hidden" class="check_download3" name="check_disgfw_domains"> 	
	</form>

<script>
var app = {};

app.download = function (){
	$(".check_download1").val($("textarea#check_gfw_domains").val());
	$(".check_download2").val($("textarea#check_nogfw_domains").val());
	$(".check_download3").val($("textarea#check_disgfw_domains").val());
	$("#downloadForm").submit();
	//window.location.href= "download.php?file="+$('#download').val();
}

var pass=0;
var forbid=0;
var notSure=0;

app.view = function(json, type){
	$("#static_div").show();

	if (type == 'wall')	{
		if(json.data.gfw){
			$.each(json.data.gfw.split("  "),function (index,domain){
				$("#check_gfw_domains").append(domain + '\n');
			});
		}
		if(json.data.nogfw){
			$.each(json.data.nogfw.split("  "),function (index,domain){
				$("#check_nogfw_domains").append(domain + '\n');
			});
		}
		if(json.data.disgfw){
			$.each(json.data.disgfw.split("  "),function (index,domain){
				$("check_disgfw_domains").append(domain + '\n');
			});
		}
		pass = pass + json.data.nogfwnum;
		forbid = forbid + json.data.gfwnum;
		notSure = notSure + json.data.disgfwnum;
	}
	else{

		$.each(json.pass,function (index,domain){
			$("#check_nogfw_domains").append(domain + '\n');
		});
		$.each(json.forbid,function (index,domain){
			$("#check_gfw_domains").append(domain + '\n');
		});
		pass = pass + (json.pass).length;
		forbid = forbid + (json.forbid).length;
	}

	$('#check_gfw_domains_status').html(forbid);
	$('#check_nogfw_domains_status').html(pass);
	$('#check_disgfw_domains_status').html(notSure);
	$('.process-span .nums').text(forbid+ pass + notSure);

	/*
	var count = 0;

	count = textArray = $("#check_gfw_domains").val().split('\n').filter(function (val) {
		return $.trim(val) ? true : false;
	}).length;
	$('#check_gfw_domains_status').html(count);
	var count1 = count;

	count = textArray = $("#check_nogfw_domains").val().split('\n').filter(function (val) {
		return $.trim(val) ? true : false;
	}).length;
	$('#check_nogfw_domains_status').html(count);
	var count2 = count;

	count = textArray = $("#check_disgfw_domains").val().split('\n').filter(function (val) {
		return $.trim(val) ? true : false;
	}).length;
	$('#check_disgfw_domains_status').html(count);
	var count3 = count;
	$('.process-span .nums').text(count1+ count2 + count3);
	*/

}
app.process = function (type){
	$("#iframe").attr( "src", "processDomains.php?type=" + type);
}
app.submit = function (type){

	$('.process-span').show();
	if (type == 'wall')	$("#tpxx").html("被墙");
	else $("#tpxx").html("被污染");
	$('#check_gfw_domains').text('');
	$('#check_nogfw_domains').text('');
	$('check_disgfw_domains').text('');
	$('.process-span .nums').html("0");
	$("#static_div").hide();
	$('.my-group button').attr('disabled',true);
	//$('#check_gfw_domains,#check_nogfw_domains,#check_disgfw_domains').val('');

	$.ajax({
		type: 'post',
		url: 'saveDomains.php',
		dataType: 'json',
		data: {
			text: $("#fname").val(),
			type: type,
		},
		success:function(data){
			if (data.status){
				app.process(type);
			}
		},
		error:function(data){
			console.log(data);		
		}
	});
}




	$(function(){
		var textArray = [];
		$(document).on('blur','#fname',function (){
			textArray = $(this).val().split('\n').filter(function (val) {
				return $.trim(val) ? true : false;
			});
			$('#dnsCount').html(textArray.length);
		});
		$('#fname').trigger('blur');
	});



</script>
</body>
</html>