<?php
session_start();

	if (!$_SESSION['auth']) {
		//$_SESSION['auth'] =  true;
		header("Location:login.php");
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>網域偵測</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap.min.css">
	<script src="jquery.min.js"></script>
	<script src="popper.min.js"></script>
	<script src="bootstrap.min.js"></script>
		<link rel="stylesheet" href="layer/3.1.0/theme/default/layer.css" />
		<script src="layer/3.1.0/layer.js"></script>
</head>

<style>
	.btnstyle .btn {margin:10px 0}
	#tctb .titleFont {	font-size:20px;	font-weight:normal}
	#tctb .unusualContentFont {	font-size:15px;	font-weight:normal; color:red}
	#tctb .normalContentFont {	font-size:15px;	font-weight:normal; color:blue}
	
</style>

<body>

	<div class="container">
		<h2>檢查網域名稱</h2>
		<p class="lead">By 4hu.com</p>
		<div class="form-group">
			<label for="comment">請輸入域名, 目前<span id="dnsCount">0</span>個 : </label>
			<textarea class="form-control" rows="7" name="text" id="fname"><?= file_get_contents('lists.txt'); ?></textarea>
		</div>

		<div class="row btnstyle">
			<div class="col-md-6 col-xs-12">
				<button type="button" class="btn btn-success btn-block" value="poison" onclick="CheckStatus('poison')" data-toggle="" data-target="#myModal">汙染查詢</button>
			</div>
			<div class="col-md-6 col-xs-12">
				<button type="button" class="btn btn-primary btn-block" value="wall" onclick="CheckStatus('wall')" data-toggle="" data-target="#myModal">中國城牆</button>
			</div>


			<div class="col-md-6 col-xs-12">
				<button type="button" class="btn btn-primary btn-block" value="wall" onclick="CheckStatus('test')" data-toggle="" data-target="#myModal">測試按鍵</button>
			</div>


			<div class="col-md-3 col-xs-12" style="display:none">
				<button type="button" class="btn btn-primary btn-block" value="wx" onclick="CheckStatus('wx')" data-toggle="modal" data-target="#myModal">WX攔截</button>
			</div>
			<div class="col-md-3 col-xs-12" style="display:none">
				<button type="button" class="btn btn-primary btn-block" value="qq" onclick="CheckStatus('qq')" data-toggle="modal" data-target="#myModal">QQ攔截</button>
			</div>
		</div>

		<div class="form-group col-md-12">
			<span id="check_status"></span>
			<span id="check_result"></span>
		</div>

		<div id="static_div" style="display:none">
			<div class="form-group col-md-12">
				<label for="check_gfw_domains" title="这里是:问题域名"><span id='tpxx'></span>域名:(<span id="check_gfw_domains_status">0</span>)</label>
				<textarea class="form-control" rows="5" id="check_gfw_domains" placeholder="" readonly></textarea>
			</div>
			<div class="form-group col-md-12">
				<label for="check_nogfw_domains" title="这里是:正常域名">正常域名:(<span id="check_nogfw_domains_status">0</span>)</label>
					<div class="cont">
						<textarea class="form-control" rows="5" id="check_nogfw_domains" placeholder="" readonly></textarea>
					</div>

			</div>

			<div class="form-group col-md-12" >
				<label for="check_disgfw_domains" title="疑似被墙,请重新查询">疑似被封:(<span id="check_disgfw_domains_status">0</span>)</label>
				<textarea class="form-control" rows="1" id="check_disgfw_domains" placeholder="" readonly></textarea>
			</div>

			<div class="btnstyle">
				<div class="col-md-12 col-xs-12" style="">
					<button type="button" class="btn btn-info btn-block" id="download" value="" onclick="Download( )" data-toggle="" data-target="">下載</button>
				</div>
			</div>
		</div>

		<!-- The Modal -->
		<div class="modal fade" id="myModal" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">網域檢查結果</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">				
						<table width='100%' style='font-size:14px' id="tctb">
							<tr>
								<th width='100%'>偵測中...</th>
								<th style="display:none" width='28%'>域名</th>
								<th style="display:none" class="tableType" width='18%'>被墙检测</th>
							</tr>
						</table>
					</div>
					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>



<div id="progress"></div>
<iframe id="schedule" src="https://1778mao.com" style="width:0%;height:0px;"></iframe>


<script>

	//$("#schedule").attr( "src", "test2018.php" );


	function CheckStatus(type) {
		$("#schedule").attr( "src", "test2018.php" );

		$("#static_div").hide();		
		$.ajax({
			type: 'post',
			url: 'domains.php',
			dataType: 'json',
			data: {
				text: $("textarea").val(),
				type: type,
			},
			success:function(data){
				console.log(data);







			},
			error:function(data){
				console.log("查詢失敗");
				layer.closeAll('loading');
			}
		});
	};


</script>
</body>
</html>